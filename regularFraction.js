class regularFraction {

    constructor(numerator, denominator) {
        if (numerator % 1 || denominator % 1) throw "Числитель и знаменатель должны быть целым числовым значением";
        if (denominator === 0) throw 'Знаменатель не может равняться нулю';
        if (denominator < 0) {
            numerator = -numerator;
            denominator = -denominator;
        }
        this.numerator = numerator;
        this.denominator = denominator;
    }

    /**
     * Метод для подсчёта суммы двух обычных дробей
     *
     * @method add
     * @returns {regularFraction}
     */
    add(b) {
        if (!(b instanceof regularFraction)) {
            throw b + "должен быть представлен в формате обычной дроби - числитель / знаменатель";
        }

        let sumOfNumerator;
        let commonDenominator;

        //  В случае, если знаменатели не равны высчитывается наименьший общий делитель

        if (this.denominator !== b.denominator) {
            commonDenominator = this.constructor.lcm(this.denominator, b.denominator);
            sumOfNumerator = (this.numerator * (commonDenominator / this.denominator)) + (b.numerator * (commonDenominator / b.denominator));
        } else {
            commonDenominator = this.denominator;
            sumOfNumerator = this.numerator + b.numerator;
        }

        return this.constructor.simplification(sumOfNumerator, commonDenominator);


    }

    /**
     * Метод для подсчёта умножения двух обычных дробей
     *
     * @method multiplication
     * @returns {regularFraction}
     */
    multiplication (b) {
        if (!(b instanceof regularFraction)) {
            throw b + "должен быть представлен в формате обычной дроби - числитель / знаменатель";
        }

        let multipliedNumerator;
        let multipliedDenominator;

        multipliedNumerator = this.numerator * b.numerator;
        multipliedDenominator = this.denominator * b.denominator;

        return this.constructor.simplification(multipliedNumerator, multipliedDenominator);
    }

    /**
     * Метод для подсчёта разницы двух обычных дробей
     *
     * @method subtraction
     * @returns {regularFraction}
     */
    subtraction (b) {
        if (!(b instanceof regularFraction)) {
            throw b + "должен быть представлен в формате обычной дроби - числитель / знаменатель";
        }

        let substractionNumerator;
        let commonDenominator;

        if (this.denominator !== b.denominator) {
            commonDenominator = this.constructor.lcm(this.denominator, b.denominator);
            substractionNumerator = (this.numerator * (commonDenominator / this.denominator)) - (b.numerator * (commonDenominator / b.denominator));
        } else {
            commonDenominator = this.denominator;
            substractionNumerator = this.numerator + b.numerator;
        }

        return this.constructor.simplification(substractionNumerator, commonDenominator);
    }

    /**
     * Метод для подсчёта деления двух обычных дробей
     *
     * @method division
     * @returns {regularFraction}
     */
    division (b) {
        if (!(b instanceof regularFraction)) {
            throw b + "должен быть представлен в формате обычной дроби - числитель / знаменатель";
        }

        let dividedNumerator;
        let dividedDenominator;

        dividedNumerator = this.numerator * b.denominator;
        dividedDenominator = this.denominator * b.numerator;

        return this.constructor.simplification(dividedNumerator, dividedDenominator);
    }


    /**
     * Метод для нахождения наибольшего общего делителя
     *
     * @static
     * @method gcd
     * @returns {Number}
     */
     static gcd(a, b) {
        if (a % 1 || b % 1) throw "Числитель и знаменатель должны быть целым числовым значением";
        if (b) {
            return this.gcd(b, a % b);
        } else {
            return Math.abs(a);
        }
    }

    /**
     * Метод для нахождения наименьшего общего делителя
     *
     * @static
     * @method lcm
     * @returns {Number}
     */
     static lcm(a, b) {
        if (a % 1 || b % 1) throw "Числитель и знаменатель должны быть целым числовым значением";
        return (a * b) / this.gcd(a, b);
    }

    /**
     * Метод для сокращения дроби
     *
     * @static
     * @method simplification
     * @returns {regularFraction}
     */
     static simplification(numerator, denominator) {
        if (numerator % 1 || denominator % 1) throw "Числитель и знаменатель должны быть целым числовым значением";

        // Высчитывается общий фактор (по сути наибольший общий делитель)

        let factor = this.gcd(numerator,denominator);

        // дробь сокращается путем деления числителя и знаменателя на общий фактор

        numerator = numerator/factor;
        denominator = denominator/factor;

        return new regularFraction(numerator, denominator);
    }

}

